import json


class ClientModel(object):
    clients = None

    def get_legal_screen(self):
        return list(filter(lambda s: s.screen.get("x") is not None, self.clients))

    @staticmethod
    def dpi_to_ppcm(dpi):
        return dpi * 0.3937

    def get_extreme_xy(self):
        screens = self.get_legal_screen()
        if not len(screens) > 0:
            return [
                [0, 0],
                [0, 0]
            ]
        return [
            [
                min(list(map(lambda s: float(s.screen["x"]), screens))),
                min(list(map(lambda s: float(s.screen["y"]), screens)))
            ],
            [
                max(list(map(lambda s: float(s.screen["x"]) + (s.screen["width"] / s.screen["ppcm"]), screens))),
                max(list(map(lambda s: float(s.screen["y"]) + (s.screen["height"] / s.screen["ppcm"]), screens)))
            ],
        ]

    def get_screen_width_height(self):
        extrema = self.get_extreme_xy()
        print(extrema)
        return [
            extrema[1][0] - extrema[0][0],
            extrema[1][1] - extrema[0][1],

        ]

    def __init__(self):
        self.clients = list()

    def add_client(self, client):
        if client not in self.clients:
            self.clients.append(client)

    def get_clients(self):
        return self.clients

    def rem_client(self, client):
        if client in self.clients:
            self.clients.remove(client)

    def broadcast_updates(self):
        for c in self.clients:
            print(c.screen)

        extrema = self.get_extreme_xy()
        video_dimensions = self.get_screen_width_height()

        for c in self.get_legal_screen():
            message = json.dumps({
                "x": 0 - ((float(c.screen["x"]) - extrema[0][0]) * c.screen["ppcm"]),
                "y": 0 - ((float(c.screen["y"]) - extrema[0][1]) * c.screen["ppcm"]),

                "width": video_dimensions[0] * c.screen["ppcm"],
                "height": video_dimensions[1] * c.screen["ppcm"],
            }).encode("UTF-8")

            print(message)
            c.sendMessage(message)
