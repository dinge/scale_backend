import json
from autobahn.twisted.websocket import WebSocketServerProtocol

from dependencies import get__client_model
from models.ClientModel import ClientModel


class ScaleServerProtocol(WebSocketServerProtocol):
    client_model = None

    def __init__(self):
        super().__init__()
        self.client_model = get__client_model()
        self.screen = {}

    def onOpen(self):
        print("alarm")
        self.client_model.add_client(self)

    def update_screen_info(self, payload):
        d = json.loads(payload.decode("UTF-8"))
        print(d)
        ppcm = ClientModel.dpi_to_ppcm(float(d["dpi"]))
        d.update({
            "ppcm": ppcm,
            "width_m": float(d["width"]) / ppcm,
            "height_m": float(d["height"]) / ppcm,
        })
        self.screen = d

    def onMessage(self, payload, isBinary):
        self.update_screen_info(payload)
        self.client_model.broadcast_updates()

    def onClose(self, wasClean, code, reason):
        self.client_model.rem_client(self)
        self.client_model.broadcast_updates()
