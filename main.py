from twisted.python import log

from models.Protocol import ScaleServerProtocol
from twisted.internet import reactor
from autobahn.twisted.websocket import WebSocketServerFactory

if __name__ == "__main__":
    import sys
    log.startLogging(sys.stdout)
    factory = WebSocketServerFactory()
    factory.protocol = ScaleServerProtocol

    reactor.listenTCP(10001, factory)
    reactor.run()
